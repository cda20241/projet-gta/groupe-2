package org.ludo;
public enum CouleurVetements {
    NOIR(1),
    BLANC(2),
    ROUGE(3),
    BLEU(4),
    VERT(5);
    //      GETTER      //
    public int getIndex() {
        return index;
    }
    private final int index; // Ajout de l'index associé à chaque type de vêtement pour pouvoir ensuite les trier
    //      CONSTRUCTEUR         //
    CouleurVetements(int index) {
        this.index = index;
    }

}
