package org.ludo;

import java.util.Scanner;
import static org.ludo.Main.getJoueur;

public class Garagiste extends Personnage implements Metier {
    // Déclaration des constantes de tarifs
    private static final int  TARIFCARROSSERIE = 200;
    private static final int  TARIFPEINTURE = 100;
    private static final int  TARIFMOTEUR = 300;

    public Garagiste(String pseudo, String sexe, Integer taille, Integer longueurCheveux) {
        super(pseudo, sexe, taille, longueurCheveux);
    }
    /** AFFICHE LE MENU DES COMPETENCES DU GARAGISTE
     *
     */
    @Override
    public void listerCompetences() {

        String competences = "Bonjour " + getJoueur().getPseudo() + ", que désirez-vous ? Je peux :";
        competences += "\n> Réparer votre carrosserie de voiture : [voiture]";
        competences += "\n> Réparer votre moteur : [moteur]";
        competences += "\n> Repeindre votre voiture : [peinture]";
        System.out.println(competences);
    }
    /**
     * Répare ou peint
     * param: l'identifiant du travail a effectuer
     */
    @Override
    public void effectuerLeTravail(String id) {

        if (id.equals("voiture")) {
            reparerCarrosserie();
            return;
        }
        if (id.equals("moteur")) {
            reparerMoteur();
            return;
        }
        if (id.equals("peinture")) {
            refairePeinture();
            return;
        }
        System.out.println("Soyez précis, cette demande ne fait pas partie de nos compétences.");
    }
    /**
     * Facture le client.
     * param: le prix à enlever du portefeuille du joueur
     */
    @Override
    public void facturerClient(Integer tarif) {
        getJoueur().setArgent(getJoueur().getArgent() - tarif);
        System.out.println("Merci "+ getJoueur().getPseudo() + ", ce service vous a été facturé " + tarif + " $.");
    }
    /**
     * Répare la carrosserie du véhicule
     */
    public void reparerCarrosserie() {
        if (getJoueur().getArgent() < TARIFCARROSSERIE) {
            getJoueur().getVehiculeConduit().setSanteCarrosserie(100);
            facturerClient(TARIFCARROSSERIE);
        } else {
            System.out.println("Vous n'avez pas assez d'argent pour réparer votre carrosserie.");
        }
    }
    /**
     * Répare le moteur du véhicule
     */
    public void reparerMoteur() {
        if (getJoueur().getArgent() < TARIFMOTEUR) {
            getJoueur().getVehiculeConduit().getMoteur().setSanteMoteur(100);
            facturerClient(TARIFMOTEUR);
        } else {
            System.out.println("Vous n'avez pas assez d'argent pour réparer votre moteur.");
        }
    }
    /**
     **  Repeint le véhicule
     * a voir: mettre l'énum des couleurs pour limiter le choix
     **/
    public void refairePeinture() {
        if (TARIFPEINTURE > getJoueur().getArgent()) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Demandez la couleur que vous souhaitez > ");
            String couleur = scan.nextLine();
            getJoueur().getVehiculeConduit().setCouleur(couleur);
            facturerClient(TARIFPEINTURE);
        } else {
            System.out.println("Vous n'avez pas assez d'argent pour repeindre votre voiture.");
        }
    }
}