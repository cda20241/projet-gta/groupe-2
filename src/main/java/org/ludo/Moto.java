package org.ludo;

public class Moto extends Vehicule {
    public Moto(String marque, String modele, String couleur, int santeCarrosserie, int santeMoteur, String proprietaire) {
        super(marque, modele, couleur, santeCarrosserie,santeMoteur, proprietaire);
    }
    /**
     * Méthode qui fait tomber la moto dans l'eau mais ne pert pas ses points de vie car c'est une moto...
     **/
    @Override
    public void tombeDansLeau(){
        if(verificationVehicule() && verifierConducteur()) {
            this.getMoteur().setSanteMoteur(this.getMoteur().getSanteMoteur());
            System.out.println("Votre véhicule est dans l'eau.");
        }
    }
}
