package org.ludo;
public class Camion extends Vehicule {
    public Camion(String marque, String modele, String couleur, int santeCarrosserie, int santeMoteur, String proprietaire) {
        super(marque, modele, couleur, santeCarrosserie,santeMoteur, proprietaire);
    }
    /**
     * Méthode qui fait tomber le camion dans l'eau mais ne pert pas ses points de vie car c'est un camion...
     **/
    @Override
    public void tombeDansLeau(){
        if(verificationVehicule() && verifierConducteur()) {
            this.getMoteur().setSanteMoteur(this.getMoteur().getSanteMoteur());
            System.out.println("Votre véhicule est dans l'eau.");
        }
    }
}
