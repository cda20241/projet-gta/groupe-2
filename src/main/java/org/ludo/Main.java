package org.ludo;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import static org.ludo.Menu.*;
import static org.ludo.Serialization.*;

public class Main  implements java.io.Serializable {
    // Déclaration des variables
    private static Integer vehicule = null;
    private static Personnage joueur = null;
    private static final List<Personnage> personnageList = new ArrayList<>();
    private static List<Vehicule> vehiculeAConduireList = new ArrayList<>();
    // GETTER & SETTER utiles
    public static Integer getVehicule() {
        return vehicule;
    }
    public static void setVehicule(Integer vehicule) {
        Main.vehicule = vehicule;
    }
    public static Personnage getJoueur() {
        return joueur;
    }

    public static List<Vehicule> getVehiculeAConduireList() {
        return vehiculeAConduireList;
    }

    public static void setVehiculeAConduireList(List<Vehicule> vehiculeAConduireList) {
        Main.vehiculeAConduireList = vehiculeAConduireList;
    }

    public static List<Personnage> getPersonnageList() {
        return personnageList;
    }

    /**
     * Main, lance le jeu, si Sauvegarde/vehiculeAConduireList.ser existe, déserialise le fichier sinon on créer une liste de véhicule
     * Lancement du menu
     */
    public static void main(String[] args){
        System.out.println(ALALIGNE);
        System.out.println("Bienvenue sur GTA POO!");
        System.out.println(ALALIGNE);
        creationPersonnage();
        File fichier = new File("Sauvegarde/vehiculeAConduireList.ser");
        if (!fichier.exists()) {
            creationVehicules();
        }else vehiculeAConduireList = deserialisationVehicules();
        joueur = menuPersonnage();
        menuSelection();
    }
    /**
     * CREATION DES PERSONNAGES
     */
    public static void creationPersonnage() {
        Personnage joueur1 = new Personnage("ludo", "H", 185, 20);
        personnageList.add(joueur1);
        Coiffeur coiffeur = new Coiffeur("roberta", "F", 150, 40);
        personnageList.add(coiffeur);
        Garagiste garagiste1 = new Garagiste("Bobby", "H", 180, 5);
        Garagiste garagiste2 = new Garagiste("Georges", "H", 170, 40);
        personnageList.add(garagiste1);
        personnageList.add(garagiste2);
    }
    /**
     * CREATION DES VEHICULES
     */
    public static void creationVehicules(){
        Vehicule vehicule1 = new Voiture("Renault", "Mégane", "Blanc", Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        Vehicule vehicule2 = new Voiture("Peugeot", "405", "Noir",Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        Vehicule vehicule3 = new Bateau("Queen", "Mary 2", "Blanc", Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        Vehicule vehicule4 = new Voiture("BMW", "M4", "Blanc",Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        Vehicule vehicule5 = new Camion("Scania", "770", "Rouge",Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        Vehicule vehicule6 = new Moto("Kawasaki", "Ninja 650", "Rouge",Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        Vehicule vehicule7 = new Avion("Boeing", "747" , "bleu",Vehicule.POINTSMAX, Vehicule.POINTSMAX, null);
        vehiculeAConduireList.add(vehicule1);
        vehiculeAConduireList.add(vehicule2);
        vehiculeAConduireList.add(vehicule3);
        vehiculeAConduireList.add(vehicule4);
        vehiculeAConduireList.add(vehicule5);
        vehiculeAConduireList.add(vehicule6);
        vehiculeAConduireList.add(vehicule7);
    }
    /**
     * Fait la sérialisation
     */
    public static void sauvegarde(){
        serialisationVehicules();
        serialisationJoueur();
    }
    /**
     * Récupération d'une partie existante
     * param: pseudo , recherche si un fichier "pseudo".ser existe si c'est le cas il déserialise les fichiers
     */
    public static void recuperationPartie(String pseudo){
        File fichier = new File("Sauvegarde/"+pseudo+".ser");
        if (!fichier.exists()) {
            System.out.println("Ce pseudo n'existe pas");
            System.out.println(ALALIGNE);
            menuPersonnage();
        }else{
            joueur = deserialisationJoueur(pseudo);
            System.out.println(deserialisationJoueur(pseudo));
            menuSelection();
        }
    }
}