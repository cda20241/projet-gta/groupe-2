package org.ludo;
/**
 * L'énumération 'CoiffeurCoupes'
 */
public enum CoiffeurCoupes {
    /**
     * La coupe 'ZIDANE'
     */
    ZIDANE(Integer.valueOf("0")),
    /**
     * La coupe 'COUPE_MILITAIRE'
     */
    COUPE_MILITAIRE(Integer.valueOf("3")),
    /**
     * La coupe 'COURTS'
     */
    COURTS(Integer.valueOf("9")),
    /**
     * La coupe 'MI_LONGS'
     */
    MI_LONGS(Integer.valueOf("22")),
    /**
     * La coupe 'LONGS'
     */
    LONGS(Integer.valueOf("40"));
    /**
     * L'attribyt 'taille'
     */
    private final Integer taille;
    CoiffeurCoupes(Integer taille) {
        this.taille = taille;
    }
    Integer getTaille() {
        return this.taille;
    }
    @Override
    public String toString() {
        return "CoiffeurCoupes{" + "taille =" + this.taille + '}';
    }
}
