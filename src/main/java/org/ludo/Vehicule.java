package org.ludo;

import java.util.*;
import java.io.Serializable;
import static org.ludo.Main.*;
import static org.ludo.Menu.*;

abstract class Vehicule implements Serializable {
    // Déclaration des variables du vehicule
    private String marque;
    private String modele;
    private String couleur;
    private Integer santeCarrosserie;
    public Moteur moteur;
    public String proprietaire;
    private String conducteur;
    public String getType() {
        return type;
    }
    public void setType(String typeVehicule) {
        if(typeVehicule.contains("Voiture")) this.type="Voiture";
        else if(typeVehicule.contains("Moto")) this.type="Moto";
        else if(typeVehicule.contains("Bateau")) this.type="Bateau";
        else if(typeVehicule.contains("Avion")) this.type="Avion";
        else if(typeVehicule.contains("Camion")) this.type="Camion";
    }

    private String type;
    private boolean aLaCasse;
    public static List<String> couleursList = Arrays.asList("ROUGE", "BLEU", "VERT", "JAUNE", "BLANC","NOIR");
    public static final Integer POINTSMAX=100;
    public static final Integer POINTSMIN=0;
    // GETTER & SETTER
    public String getMarque() {
        return marque;
    }
    public String getModele() {
        return modele;
    }
    public String getProprietaire() {
        return this.proprietaire;
    }
    public Moteur getMoteur() {
        return this.moteur;
    }
    public Integer getSanteCarrosserie() {
        return this.santeCarrosserie;
    }
    public void setSanteCarrosserie(Integer santeCarrosserie) {
        this.santeCarrosserie = santeCarrosserie;
    }
    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
    public String getCouleur() {
        return couleur;
    }
    // CONSTRUCTEUR
    public Vehicule(String marque, String modele, String couleur, int santeCarrosserie, int santeMoteur, String proprietaire) {
        this.marque = marque;
        this.modele = modele;
        this.couleur = couleur;
        this.santeCarrosserie = santeCarrosserie;
        this.moteur = new Moteur(santeMoteur);
        this.proprietaire = proprietaire;
        this.setType(String.valueOf(this.getClass()));

    }
    // Methode abstraite qui fait tomber le véhicule tombe dans l'eau
    public abstract void tombeDansLeau();
    public String toString(){//overriding the toString() method
        return type+" "+marque+" "+modele+" "+couleur;
    }
    public boolean verificationVehicule() {
        /**
         * Fonction qui vérifie l'état du véhicule,
         * si les points de vie du véhicule sont à zéro, on supprime cet objet
         */
        if(this.santeCarrosserie==null || this.santeCarrosserie<=POINTSMIN ) {
            this.aLaCasse = true;
            this.marque = null;
            this.modele = null;
            this.couleur = null;
            this.proprietaire = null;
            this.conducteur = null;
            this.santeCarrosserie = null;
            this.moteur = null;
            System.out.println("Votre voiture est partie à la casse");
            getVehiculeAConduireList().remove(this);
            Menu.menuSelection();
            return false;
        }else return true;
    }
    // Simule un accident
    public void accident(Integer pointsDegat) {
        /**
         * Simule un accident
         * @param: pointCarrosserie = Integer à soustraire au total des points carrosserie
         * @param: pointMoteur = Integer à soustraire au total des points moteur
         */
        if(verifierConducteur()  && !aLaCasse){
            this.santeCarrosserie -= pointsDegat;
            if(this.santeCarrosserie<0) this.santeCarrosserie=POINTSMIN;
            this.moteur.setSanteMoteur(moteur.getSanteMoteur()-pointsDegat);

            System.out.println("Vous avez eu un accident...");
            System.out.println("Etat du véhicule:");
            if(this.santeCarrosserie==null) {
                System.out.println("Carrosserie (0/" + POINTSMAX + ")");
            }else System.out.println("Carrosserie ("+this.santeCarrosserie+"/" + POINTSMAX + ")");

            if(this.moteur == null) {
                System.out.println("Moteur: (0/" + POINTSMAX + ")");
            }else  System.out.println("Moteur: ("+moteur.getSanteMoteur()+"/" + POINTSMAX + ")");
            this.verificationVehicule();
        }
    }
    public boolean verifierConducteur(){
        /**
         * Vérifie si il y a un conducteur pour simuler un accident
         * S'il n'y pas de conducteur on ne peut pas avoir d'accident
         */
        if(this.conducteur!=null) return true;
        else {
            System.out.println("Il n'y a aucun conducteur");
            return false;
        }
    }
    // Pour désigner un conducteur
    public void conduire(){
        /**
         * Methode pour conduire un vehicule,affiche le menu conduite. // menuConduite()
         */
        if(!aLaCasse){
            // Si la santé du moteur est à 0, on ne peut pas conduire
            if (this.moteur.getSanteMoteur() <= POINTSMIN) {
                System.out.println("Vous ne pouvez pas démarrer, moteur cassé. ("+ moteur.getSanteMoteur()+"/"+POINTSMAX+")");
                // Sinon on assigne le pseudo au conducteur de la voiture
            } else {
                System.out.println("Vous venez de monter dans  "+this.marque+" "+this.modele);
                this.conducteur = getJoueur().getPseudo();
                getJoueur().setVehiculeConduit(this);
            }
        }else System.out.println("La voiture n'existe plus");
        menuConduite();
    }
    public void acheter(){
        /**
         * Methode pour acheter un véhicule.
         * Vérifie s'il y a un propriétaire. si c'est le cas retour au menu achatVehicule()
         * Sinon achète le véhicule et va au menu conduite du véhicule // menuConduite
         */
        if((!aLaCasse)) {
            if (this.proprietaire == null) {
                this.proprietaire = getJoueur().getPseudo();
                System.out.println("Merci "+ getJoueur().getPseudo()+" pour l'achat de "+this.marque+" "+this.modele);
                this.conducteur = getJoueur().getPseudo();
                getJoueur().setVehiculeConduit(this);
                getJoueur().vehiculeAcheterList.add(this);
                menuConduite();
            } else {
                System.out.println("Vous ne pouvez pas acheter ce véhicule, il appartient déjà à " + this.proprietaire);
                achatVehicule();
            }
        }else System.out.println("La voiture n'existe plus");
    }
    public void quitterVehicule(){
        /**
         * Methode pour descendre d'un véhicule et retourner au menu joueur // menuSelection()
         */
        if(verificationVehicule()) {
            System.out.println("Vous quittez "+this.marque+" "+this.modele);
            getJoueur().setVehiculeConduit(null);
            System.out.println(ALALIGNE);
        }
    }
}