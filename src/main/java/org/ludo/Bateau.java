package org.ludo;

public class Bateau extends Vehicule {
    public Bateau(String marque, String modele, String couleur, int santeCarrosserie, int santeMoteur, String proprietaire) {
        super(marque, modele, couleur, santeCarrosserie, santeMoteur, proprietaire);
    }
    /**
     * Méthode qui fait tomber le bateau dans l'eau mais ne pert pas ses points de vie car c'est un bateau (mais va sur la route...)
     **/
    @Override
    public void tombeDansLeau(){
        if(verificationVehicule() && verifierConducteur()) {
            this.getMoteur().setSanteMoteur(this.getMoteur().getSanteMoteur());
            System.out.println("Votre véhicule est dans l'eau.");
        }
    }
}
