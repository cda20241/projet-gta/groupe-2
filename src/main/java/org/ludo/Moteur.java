package org.ludo;

import java.io.Serializable;

public class Moteur implements Serializable {
    // Déclaration des variables moteur
    private Integer santeMoteur;
    private static final int POINTSMIN=0;
    // GETTER & SETTER
    public Integer getSanteMoteur() {
        return santeMoteur;
    }
    public void setSanteMoteur(int pointMoteur) {
        this.santeMoteur = Math.max(pointMoteur, POINTSMIN);
    }
    // CONSTRUCTEUR
    public Moteur(int santeMoteur) {
        this.santeMoteur = santeMoteur;
    }
}