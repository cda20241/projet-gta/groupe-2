package org.ludo;
public enum TailleVetement {
    S(1),
    M(2),
    L(3),
    XL(4),
    XXL(5),
    XXXl(6);
    private int TailleIndex; // Ajout de TailleIndex associé à chaque type de vêtement pour pouvoir ensuite les trier
    //et les afficher comme je veux
    //      CONSTRUCTEUR         //
    TailleVetement(int TailleIndex) {
        this.TailleIndex = TailleIndex;
    }
    //      GETTER      //

    public int getTailleIndex() {
        return TailleIndex;
    }
}
