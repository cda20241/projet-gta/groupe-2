package org.ludo;

import java.io.File;
import java.util.Objects;
import java.util.Scanner;
import static java.lang.Integer.parseInt;
import static java.lang.System.in;
import static org.ludo.Main.*;

public class Menu {
    public static final String ALALIGNE = " ";
    /**
     * Menu "PERSONNAGE"
     * POUR COMMENCER UNE NOUVELLE PARTIE OU CONTINUER UNE PARTIE EXISTANTE
     */
    public static Personnage menuPersonnage(){
        Scanner myobj = new Scanner(in);
        File fichier = new File("Sauvegarde/vehiculeAConduireList.ser");

        System.out.println("Tapez 1 pour commencer une nouvelle partie");
        if(fichier.exists()){
            System.out.println("Tapez 2 pour continuer votre partie");
        }
        String choix = myobj.nextLine();
        if(Objects.equals(choix, "1")){
            return menuNouvellePartie();
        }else if(Objects.equals(choix, "2")) {
            menuContinuerPartie();
            return null;
        }else{
            menuPersonnage();
            return null;
        }

        // ajouter une option pour créer le personnage
    }
    /**
     * Méthode pour continuer une partie existante
     * récupère le pseudo et vérifie si un joueur de ce nom existe et dans ce cas charge la partie
     */
    public static void menuContinuerPartie() {

        Scanner myobj = new Scanner(in);
        System.out.println("Veuillez indiquer votre pseudo pour charger votre partie:");
        System.out.println(ALALIGNE);
        String pseudo = myobj.nextLine();
        recuperationPartie(pseudo);

    }
    /**
     * Methode qui affiche le menu de sélection ou la création d'un personnage
     * dans le cas d'une nouvelle partie.
     */
    public static Personnage menuNouvellePartie() {

        Scanner myobj = new Scanner(in);
        System.out.println("### SELECTION DU PERSONNAGE ###");
        System.out.println(ALALIGNE);
        System.out.println("Veuillez sélectionner un joueur existant ou en créer un nouveau:");
        System.out.println(ALALIGNE);
        Personnage.menuListePersonnage();
        int choix = myobj.nextInt();
        // Va charger le personnage choisi
        if(choix <= getPersonnageList().size()){
            return Personnage.recuperationPersonnage(choix);
        // Va au menu de création d'une nouveau personnage.
        }else{
            return menuCreationPersonnage();
        }
    }
    /**
     * Méthode qui créer un nouveau personnage personnalisé.
     */
    public static Personnage menuCreationPersonnage(){

        Scanner myobj = new Scanner(in);
        System.out.println(ALALIGNE);
        System.out.println("### CREATION DU PERSONNAGE ###");
        System.out.println(ALALIGNE);
        System.out.println("Veuillez indiquer votre pseudo: ");
        String pseudo = myobj.nextLine();
        System.out.println("Veuillez indiquer votre sexe (H/F): ");
        String sexe = myobj.nextLine();
        if (!Objects.equals(sexe.toUpperCase(), "H") && !Objects.equals(sexe.toUpperCase(), "F")) {
            System.out.println("Veuillez entrer [H] pour un Homme, [F] pour une femme");
            sexe = myobj.nextLine();
        }
        System.out.println("Veuillez indiquer votre taille: ");
        Integer taille = myobj.nextInt();
        System.out.println("Veuillez indiquer la longueur de vos cheveux: ");
        Integer longueurCheveux = myobj.nextInt();
        return  new Personnage(pseudo, sexe, taille, longueurCheveux);
    }
    /**
     * Menu principal du joueur
     */
    public static void menuSelection(){
        System.out.println(ALALIGNE);
        System.out.println("Bonjour "+ getJoueur().getPseudo());
        System.out.println(ALALIGNE);
        Scanner myobj = new Scanner(in);
        System.out.println("### MENU JOUEUR ###");
        System.out.println(ALALIGNE);
        System.out.println("Tapez 1 pour afficher les informations du joueur");
        System.out.println("Tapez 2 pour s'habiller");
        System.out.println("Tapez 3 pour conduire un véhicule");
        System.out.println("Tapez 4 pour acheter un véhicule");
        System.out.println("Tapez 5 pour aller chez le coiffeur");
        System.out.println("Tapez 6 pour sauvegarder et sortir du jeu");
        System.out.println(ALALIGNE);
        int choix = myobj.nextInt();
        switch (choix) {
            case 1:
                menuInfosJoueur();
                break;
            case 2:
                choixVetements();
                break;
            case 3:
                conduireVehicule();
                break;
            case 4:
                achatVehicule();
                break;
            case 5:
                getJoueur().coiffeur();
                break;
            case 6:
                menuExit();
                break;
            default:
                System.out.println("Tapez 1 pour afficher les informations du joueur");
                menuSelection();
        }
    }
    /**
     * Methode qui affiche le menu des informations du joueur
     */
    public static void menuInfosJoueur(){

        Scanner myobj = new Scanner(in);
        System.out.println(ALALIGNE);
        System.out.println("### INFORMATIONS PERSONNAGE ###");
        System.out.println(ALALIGNE);
        System.out.println("Pseudo :"+ getJoueur().getPseudo());
        System.out.println("Sexe :"+ getJoueur().getSexe());
        System.out.println("Taille sans talon:"+ getJoueur().getTaille()+" cm");
        if(!getJoueur().chaussureList.isEmpty()) {
            int tailleAvecChaussure = getJoueur().getTaille() + parseInt(getJoueur().chaussureList.get(2));
            System.out.println("Taille avec talon:" + tailleAvecChaussure + " cm");
        }
        System.out.println("Longueur des cheveux :"+ getJoueur().getLongueurCheveux()+" cm");
        System.out.println(ALALIGNE);
        System.out.println("Porte-monnaie :"+getJoueur().getArgent()+" €");
        System.out.println(ALALIGNE);
        if(getJoueur().vetementList.isEmpty()) {
            System.out.println("Vous n'avez pas de vêtements");
        }else{
            System.out.println("Vêtement haut : " + getJoueur().vetementList.get(0) + " taille " + getJoueur().vetementList.get(1) + " de couleur " + getJoueur().vetementList.get(2));
            System.out.println("Vêtement bas : " + getJoueur().vetementList.get(3) + " taille " + getJoueur().vetementList.get(4) + " de couleur " + getJoueur().vetementList.get(5));
        }
        System.out.println(ALALIGNE);
        if(getJoueur().chaussureList.isEmpty()){
            System.out.println("Vous n'avez pas de chaussures");
        }else {
            System.out.println("Chaussures : "+getJoueur().chaussureList.get(0)+" de couleur "+getJoueur().chaussureList.get(1)+" - Hauteur de talon: "+getJoueur().chaussureList.get(2)+" cm");
        }
        if(!getJoueur().getVehiculeAcheterList().isEmpty()){
            System.out.println("Vous êtes proprietaire de:");
            System.out.println(getJoueur().getVehiculeAcheterList().toString());
        }
        System.out.println(ALALIGNE);
        System.out.print("Tapez sur n'importe quelle touche pour retourner au menu précédent: ");
        String choix = myobj.nextLine();
        if(Objects.equals(choix, "")) {
            menuSelection();
        } else menuSelection();
    }
    /**
     * Affiche le menu de sélection des vetements
     * **/
    public static void choixVetements(){

        Scanner myobj = new Scanner(System.in);
        System.out.println(ALALIGNE);
        Vetement.menuIntroVetements();
        Vetement.menuVetementTop();
        Vetement.menuVetementBas();
        Vetement.panoplieComplete();
        Vetement.menuOutro();
        System.out.println("Tapez Entrée pour retourner au menu précédent:");
        String choix= myobj.nextLine();
        if(Objects.equals(choix, "")) {
            menuSelection();
        }else menuSelection();
    }
    /**
     * Menu qui affiche la liste des véhicules à conduire puis va au menu conduite du véhicule // menuConduite()
     */
    public static void conduireVehicule(){

            System.out.println(ALALIGNE);
            System.out.println("### SELECTION VEHICULE ###");
            System.out.println(ALALIGNE);
            if (getVehicule() != null) {
                getVehiculeAConduireList().get(getVehicule()).quitterVehicule();
            }
            for (int i = 0; i < getVehiculeAConduireList().size(); i++) {
                System.out.println("Tapez " + i + " pour conduire " + getVehiculeAConduireList().get(i));
            }
            Scanner myobj = new Scanner(in);
            int vehiculeChoisi = myobj.nextInt();
            if(vehiculeChoisi<= getVehiculeAConduireList().size()) {
                getVehiculeAConduireList().get(vehiculeChoisi).conduire();
                System.out.println(ALALIGNE);
            }
    }
    /**
     * Menu conduite, affiche le menu d'action du véhicule.
     * Affiche le propriétaire s'il y en a un
     * Affiche les points carrosserie & moteur
     * Affiche les options, aller au garage, tomber dans l'eau, prendre un mur, et descendre du véhicule.
     */
    public static void menuConduite(){

        System.out.println(ALALIGNE);
        Scanner myobj = new Scanner(in);
        System.out.println("####### MENU "+getJoueur().getVehiculeConduit()+" #######" );
        if(getJoueur().getVehiculeConduit().getProprietaire()!=null){
            System.out.println("# Propriétaire: "+getJoueur().getVehiculeConduit().getProprietaire() );
        }
        System.out.println(ALALIGNE);
        System.out.println("# Santé Moteur: "+getJoueur().getVehiculeConduit().getMoteur().getSanteMoteur()+"#" );
        System.out.println("# Santé Carrosserie: "+getJoueur().getVehiculeConduit().getSanteCarrosserie()+"#" );
        System.out.println(ALALIGNE);
        System.out.println("Tapez 1 pour aller au garage ");
        System.out.println("Tapez 2 pour tomber dans l'eau ");
        System.out.println("Tapez 3 pour prendre un mur (-20 points) ");
        System.out.println("Tapez 4 pour descendre du véhicule ");
        int choix= myobj.nextInt();
        switch (choix) {
            case 1:
                // on va au garage
                getJoueur().garagiste();
                menuConduite();
                break;
            case 2:
                // tombe dans l'eau
                getJoueur().getVehiculeConduit().tombeDansLeau();
                menuConduite();
                break;
            case 3:
                // on créer un accident
                if (getJoueur().getVehiculeConduit().getMoteur().getSanteMoteur() > 0) {
                    getJoueur().getVehiculeConduit().accident(20);
                } else System.out.println("Moteur cassé, vous ne pouvez plus avancer");
                menuConduite();
                break;
            case 4:
                // on quitte le vehicule
                getJoueur().getVehiculeConduit().quitterVehicule();
                menuSelection();
                break;
            default:
                // si erreur de saisie, retourne à ce menu
                menuConduite();
                System.out.println(ALALIGNE);
                break;
        }
    }
    /**
     * Menu qui affiche la liste des vehicules du jeu
     * (a faire: voir pour afficher que les véhicules sans propriétaire)
     */
    public static void achatVehicule(){

        System.out.println("### CONCESSIONNAIRE ###");
            System.out.println(ALALIGNE);
        int sortie=0;
        for(int i=0; i<getVehiculeAConduireList().size(); i++){
            if(!Objects.equals(getVehiculeAConduireList().get(i).getProprietaire(), "")) {
                System.out.println("Tapez " + i + " pour acheter " + getVehiculeAConduireList().get(i));
            }
            sortie = i;
        }
        sortie++;
        System.out.println("Tapez " + sortie + " pour revenir au menu précédent");
        Scanner myobj = new Scanner(in);
        int choixVehicule = myobj.nextInt();
        if(choixVehicule<getVehiculeAConduireList().size()) {
            getVehiculeAConduireList().get(choixVehicule).acheter();
            menuConduite();
        }else menuSelection();
    }
    /**
     * Sauvegarde la partie en cours puis quitte le jeu
     */
    public static void menuExit(){

        sauvegarde();
        System.out.println("Voulez vous sortir du jeu ? (oui/non)");
        Scanner scanner = new Scanner(System.in);
        String sortie = scanner.nextLine();
        if (sortie.equals("oui")) {
            System.out.println("Merci d'avoir joué à cette superbe version de GTA ");
            System.out.println("Copyright CDA22 - Ludovic, Neven, Fabien");
            System.exit(5);
        }
    }
}