package org.ludo;
public enum TypeVetement {
    PULL("top", 0 ),
    JEAN("bas", 1 ),
    TSHIRT("top", 2),
    SHORT("bas", 3);
    private String categorie; // Ajout de la catégorie associée à chaque type de vêtement pour pouvoir ensuite les trier
    private int numero; // Ajout du numéro associé à chaque type de vêtement pour pouvoir ensuite les trier
    //      CONSTRUCTEUR         //
    TypeVetement(String categorie, int numero) {
        this.categorie = categorie;
        this.numero = numero;
    }
    //      GETTER      //
    public String getCategorie() {
        return categorie;
    }
    public int getNumero() {
        return numero;
    }
}