package org.ludo;

import java.io.*;
import java.util.List;
import static org.ludo.Main.*;

public class Serialization {
    /**
     * Serialise la liste de véhicules
     */
        public static void serialisationVehicules(){
            try (FileOutputStream fichierDeSortie = new FileOutputStream("sauvegarde/vehiculeAConduireList.ser");
                 ObjectOutputStream sortieObjet = new ObjectOutputStream(fichierDeSortie)) {
                sortieObjet.writeObject(getVehiculeAConduireList());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    /**
     * Serialise les données du joueur
     */
        public static void serialisationJoueur() {
            try (FileOutputStream fichierDeSortie = new FileOutputStream("sauvegarde/"+getJoueur().getPseudo()+".ser");
                 ObjectOutputStream sortieObjet = new ObjectOutputStream(fichierDeSortie)) {
                sortieObjet.writeObject(getJoueur());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    /**
     * Désérialise la liste des véhicules
     * return: la liste des véhicules du jeu (List<Vehicule>)
     */
        public static List<Vehicule> deserialisationVehicules() {

            try (FileInputStream fichierEntree = new FileInputStream("sauvegarde/vehiculeAConduireList.ser");
                 ObjectInputStream entreeObjet = new ObjectInputStream(fichierEntree)) {
                 setVehiculeAConduireList((List<Vehicule>) entreeObjet.readObject());
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return getVehiculeAConduireList();
        }
    /**
     * Désérialise les données du joueur
     * return: l'objet joueur (Personnage)
     */
        public static Personnage deserialisationJoueur(String pseudo) {

            Personnage joueur = null;
            try (FileInputStream fichierEntree = new FileInputStream("sauvegarde/"+pseudo+".ser");
                 ObjectInputStream entreeObjet = new ObjectInputStream(fichierEntree)) {
                joueur = (Personnage) entreeObjet.readObject();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return joueur;
        }
}