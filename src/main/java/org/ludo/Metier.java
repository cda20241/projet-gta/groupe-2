package org.ludo;
public interface Metier {
    // La méthode "listerCompetences" affiche les services d'un prestataire.
    void listerCompetences();
    // La méthode "effectuerLeTravail" effectue une compétence demandée par rapport à son identifiant.
    // @param id correspond à l'identifiant du travail à réaliser par le professionnel.
    void effectuerLeTravail(String id);
    // La méthode "facturerClient" débite le client du prix d'un service et lui affiche une facture.
    // @param tarif correspond au prix à payer.
    void facturerClient(Integer tarif);
}
