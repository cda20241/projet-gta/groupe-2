package org.ludo;

import java.util.Scanner;

class Voiture extends Vehicule{
    public Voiture(String marque, String modele, String couleur, int santeCarrosserie, int santeMoteur, String proprietaire) {
        super(marque, modele, couleur, santeCarrosserie, santeMoteur, proprietaire);
    }
    /**
     * Méthode qui descents les points de vie du moteur à 0 comme c'est une voiture
     */
    @Override
    public void tombeDansLeau(){

        if(verificationVehicule() && verifierConducteur()){
            getMoteur().setSanteMoteur(POINTSMIN);
            System.out.println("Votre véhicule est dans l'eau, moteur noyé ("+getMoteur().getSanteMoteur()+"/"+POINTSMAX+")");
        }
    }
    /**
     * Méthode qui affiche le menu de réparation d'un garagiste
     * a voir: afficher un menu de sélection d'un garagiste
     */
    public static void garagiste() {

        Garagiste garagiste = new Garagiste("paolo", "H", 185, 15);
        garagiste.listerCompetences();
        Scanner scan = new Scanner(System.in);
        System.out.println("Écrivez l'identifiant de votre demande > ");
        String competence = scan.nextLine();
        garagiste.effectuerLeTravail(competence);
    }
}
