package org.ludo;

import java.util.Objects;
import java.util.Scanner;
import static org.ludo.Menu.menuSelection;

public class Coiffeur extends Personnage implements Metier {
    private Personnage client;

    public Coiffeur(String pseudo, String sexe, Integer taille, Integer longueurCheveux) {
        super(pseudo, sexe, taille, longueurCheveux);
    }

    /**
     * Le constructeur
     * @return Objet de classe "Coiffeur".
     */
    public static Coiffeur createCoiffeur() {
        return new Coiffeur("roberta", "F", 110, 40);
    }
    /**
     * Le getter
     * @return L'attribut "client"
     */
    public Personnage getClient() {
        return this.client;
    }
    /**
     * Le setter
     * @param client Le nouvel attribut "client"
     */
    public void setClient(Personnage client) {
        this.client = client;
    }

    @Override
    public void listerCompetences() {
        String competences = "Bonjour " + this.getClient().getPseudo() + ",  que désirez-vous ? Je peux :";
        competences += "\n> Couper vos cheveux : [COUPE]";
        competences += "\n> Poser des extensions : [EXTENSIONS]";
        System.out.println(competences);
    }

    /**
     * La méthode
     * @param id correspond à l'identifiant 'String' du travail à réaliser par le coiffeur.
     */
    @Override
    public void effectuerLeTravail(String id) {
        Scanner scan = new Scanner(System.in);
        if (id.equalsIgnoreCase("COUPE")) {
            faireCoupe();
            return;
        }else if(id.equalsIgnoreCase("EXTENSIONS")) {
            poserExtensions();
            return;
        }
        System.out.println("Soyez précis, cette demande ne fait pas partie de nos compétences.");
        System.out.println("Tapez entrée pour retourner au menu principal");
        String choix = scan.nextLine();
        if(Objects.equals(choix, "")){
            menuSelection();
        }
    }

    /**
     * La méthode
     * @param tarif correspond au prix 'Integer' à payer.
     */
    @Override
    public void facturerClient(Integer tarif) {
        this.getClient().setArgent(this.getClient().getArgent() - tarif);
        System.out.println(this.getClient().getPseudo() + ", ce service est facturé " + tarif + " $.");
    }

    /**
     * La méthode
     */
    public void faireCoupe() {
        if (this.getClient().getLongueurCheveux() > 0) {
            StringBuilder coupes = new StringBuilder("Nous pouvons vous proposer ces coupes :");
            for (CoiffeurCoupes coupe : CoiffeurCoupes.values()) {
                if (this.getClient().getLongueurCheveux() > coupe.getTaille()) {
                    String nomCoupe = coupe.name();
                    Integer tailleCoupe = coupe.getTaille();
                    coupes.append("\n> [");
                    coupes.append(nomCoupe);
                    coupes.append("] : ");
                    coupes.append(tailleCoupe);
                    coupes.append(" cm");
                }
            }
            System.out.println(coupes);
            Scanner scan = new Scanner(System.in);
            System.out.println("Laquelle vous choississez ? > ");
            String coupe = scan.nextLine();
            try {
                Integer taille = this.getClient().getLongueurCheveux() - CoiffeurCoupes.valueOf(coupe).getTaille();
                if (taille < getClient().getArgent()) {
                    switch (CoiffeurCoupes.valueOf(coupe)) {
                        case ZIDANE -> this.getClient().setLongueurCheveux(CoiffeurCoupes.ZIDANE.getTaille());
                        case COUPE_MILITAIRE -> this.getClient().setLongueurCheveux(CoiffeurCoupes.COUPE_MILITAIRE.getTaille());
                        case COURTS -> this.getClient().setLongueurCheveux(CoiffeurCoupes.COURTS.getTaille());
                        case MI_LONGS -> this.getClient().setLongueurCheveux(CoiffeurCoupes.MI_LONGS.getTaille());
                        case LONGS -> this.getClient().setLongueurCheveux(CoiffeurCoupes.LONGS.getTaille());
                    }
                    facturerClient(taille);
                    System.out.println("Tapez entrée pour retourner au menu principal");
                    String choix = scan.nextLine();
                    if(Objects.equals(choix, "")){
                        menuSelection();
                    }
                } else {
                    System.out.println("Vous n'avez pas assez d'argent pour cette coupe de cheveux.");
                    menuSelection();
                }
            } catch (IllegalArgumentException exception) {
                System.out.println("La coupe demandée n'était pas proposée.");
                menuSelection();
            }
        } else {
            System.out.println("Nous ne pouvons pas faire de coupes, vos cheveux sont déjà rasés.");
            menuSelection();
        }
    }



    /**
     * La méthode
     */
    public void poserExtensions() {
        if (this.getClient().getLongueurCheveux() < 60) {
            int minExt = 1;
            int maxExt = Math.min(60 - this.getClient().getLongueurCheveux(), 40);
            Scanner scan = new Scanner(System.in);
            System.out.println("Demandez une taille (cm) d'extensions entre " + minExt + " et " + maxExt + ". > ");
            Integer taille = scan.nextInt();
            if (taille >= minExt && taille <= maxExt) {
                if (this.getClient().getArgent() >= taille) {
                    this.getClient().setLongueurCheveux(this.getClient().getLongueurCheveux() + taille);
                    facturerClient(taille);
                    System.out.println("Tapez entrée pour retourner au menu principal");
                    String choix = scan.nextLine();
                    if(Objects.equals(choix, "")){
                        menuSelection();
                    }
                } else {
                    System.out.println("Vous n'avez pas assez d'argent pour poser des extensions.");
                    menuSelection();
                }
            } else {
                System.out.println("La taille des extensions demandée n'était pas proposée.");
                poserExtensions();
            }
        } else {
            System.out.println("Nous ne pouvons pas poser d'extensions, vos cheveux sont à une longueur maximale.");
            menuSelection();
        }
    }
}