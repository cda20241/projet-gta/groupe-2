package org.ludo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

import static org.ludo.Main.*;

public class Personnage implements Serializable {
    private String pseudo;
    private String sexe;
    private Integer taille;
    private Integer longueurCheveux;
    private Integer argent;
    protected List<String> vetementList = new ArrayList<>();
    protected List<String> chaussureList = new ArrayList<>();

    public List<Vehicule> getVehiculeAcheterList() {
        return vehiculeAcheterList;
    }

    public void setVehiculeAcheterList(List<Vehicule> vehiculeAcheterList) {
        this.vehiculeAcheterList = vehiculeAcheterList;
    }

    protected List<Vehicule> vehiculeAcheterList = new ArrayList<>();
    private Vehicule vehiculeConduit = null ;
    private static final Integer ARGENTDEBUT = 10000;
// GETTER & SETTER
    public Vehicule getVehiculeConduit() {
        return vehiculeConduit;
    }
    public void setVehiculeConduit(Vehicule vehiculeConduit) {
        this.vehiculeConduit = vehiculeConduit;
    }
    public void setLongueurCheveux(Integer longueurCheveux) {
        this.longueurCheveux = longueurCheveux;
    }
    public void setArgent(Integer argent) {
        this.argent = argent;
    }

    public String getPseudo() {
        return pseudo;
    }
    public String getSexe() {
        return sexe;
    }
    public Integer getTaille() {
        return taille;
    }
    public Integer getLongueurCheveux() {
        return longueurCheveux;
    }
    public Integer getArgent() {
        return argent;
    }
    public List<String> getVetementsList() {
        return vetementList;
    }
    public List<String> getChaussuresList() {
        return chaussureList;
    }

    public Personnage(String pseudo, String sexe, Integer taille, Integer longueurCheveux) {
        this.pseudo = pseudo;
        this.sexe = sexe;
        this.taille = taille;
        this.longueurCheveux = longueurCheveux;
        this.argent = ARGENTDEBUT;
    }

    public static void rechercherMetier(String metier){
        // Fonction pour savoir il y a combien il y a de personnage qui exerce tel métier
        int nbr = 0;
        for (Personnage personnage : getPersonnageList()) {
            if (personnage.getClass().getName().contains(metier)) {
                nbr++;
            }
        }
        if(Objects.equals(metier, "Personnage")){
            System.out.println("Il y a "+nbr+" joueur(s)");
        }else System.out.println("Il y a "+nbr+" "+metier+"(s)");
    }
    public static void menuListePersonnage(){
        int nbr = 0;
        for (Personnage personnage : getPersonnageList()) {
            String typePersonnage="";
            nbr++;

            if(personnage.getClass().getName().contains("Garagiste")){
                typePersonnage = "garagiste";
            }
            else if(personnage.getClass().getName().contains("Coiffeur")){
                typePersonnage = "coiffeur";
            }
            else {
                typePersonnage = "personnage";
            }
            System.out.println("Tapez "+nbr+" pour sélectionner le "+typePersonnage+" "+personnage.getPseudo() );
        }
        nbr++;
        System.out.println("Tapez "+nbr+" pour créer un nouveau personnage.");
    }
    public static Personnage recuperationPersonnage(int numeroPersonnage){
        int nbr = 1;
        Personnage joueur = null;
        for (Personnage personnage : getPersonnageList()) {
            if(numeroPersonnage==nbr) {
                    joueur = personnage;
                break;
            }else nbr++;
        }
        return joueur;
    }
    public void coiffeur() {
        Coiffeur coiffeur = Coiffeur.createCoiffeur();
        coiffeur.setClient(this);
        coiffeur.listerCompetences();
        Scanner scan = new Scanner(System.in);
        System.out.println("Écrivez l'identifiant de votre demande > ");
        String competence = scan.nextLine();
        coiffeur.effectuerLeTravail(competence);
    }

    /**
     * La méthode "garagiste"
     * @param //voiture La "Voiture" à modifier chez le garagiste.
     */
    public void garagiste() {
        if (getJoueur().getVehiculeConduit()!=null) {
            Voiture.garagiste();
        }
    }

}
