package org.ludo;

import java.util.Scanner;

public class Chaussure {
    // Déclaration des variables
    private static String couleurDesChaussures;
    private static String nomChaussure;
    private static CouleurVetements dernierChoixCouleur;
    private static int talonChaussure;
    // GETTER
    public static String getCouleurDesChaussures() {
        return couleurDesChaussures;
    }
    public static String getNomChaussure() {
        return nomChaussure;
    }
    public static CouleurVetements getDernierChoixCouleur() {
        return dernierChoixCouleur;
    }
    public static int getTalonChaussure() { return talonChaussure; }

    /**Cette méthode sert à définir quel type de chaussures le personnage porte. De fait, elle définit la
     * hauteur du talon des chaussures portées. Cette hauteur de talon servira ensuite pour calculer
     * la taille totale du personnage
     */
    public static void talonChaussures() {
        System.out.println("Quel type de chaussures porte le personnage ?");
        System.out.println("1. Chaussures plates  | 2. Sneakers - baskets | 3. Chaussures de ville | 4. Talons hauts");
        Scanner scanner = new Scanner(System.in);
        int chaussuresPortees = scanner.nextInt();
        // après avoir proposé un choix à l'utilisateur, j'utilise un switch/case pour définir la hauteur de talon et
        //sauvegarder le type de chaussures portées par le personnage
        switch (chaussuresPortees) {
            case 1:
                talonChaussure = 0;
                nomChaussure = "Chaussures plates";
                break;
            case 2:
                talonChaussure = 1;
                nomChaussure = "Sneakers - baskets";
                break;
            case 3:
                talonChaussure = 2;
                nomChaussure = "Chaussures de ville";
                break;
            case 4:
                talonChaussure = 10;
                nomChaussure = "Talons hauts";
                break;
            default:
                System.out.println("Choix invalide");
        }
        System.out.println("De quelle couleur voulez-vous vos chaussures ?");
        //il faut maintenant choisir la couleur des chaussures et j'appelle donc la méthode nouveauChoixCouleur()
        nouveauChoixCouleur();
        couleurDesChaussures = String.valueOf(Chaussure.dernierChoixCouleur);
    }
    /**Je créé une nouvelle méthode pour le choix de couleur. Elle propose un affichage plus optimisé et permet à l'utilsateur
     * de ne saisir que le numéro de la couleur
     */
    public static void nouveauChoixCouleur() {
        // je créé un for afin d'imprimer une liste composée d'un chiffre, un point et la valeur de la couleur. Le chiffre
        // est celui qui a été associé à la couleur dans la classe d'énumération CouleurVetements
        for (CouleurVetements e : CouleurVetements.values()) {
            System.out.print(e.getIndex() + ". " + e.name() + "   |   ");
        }
        Scanner scanner = new Scanner(System.in);
        int choixUtilisateur = scanner.nextInt();
        //Je créé un for afin de faire un compteur, et comme cela, grâce à la condition, quand le compteur arrive au même chiffre
        // que celui entré par l'utilisateur, cela sélectionne la valeur de l'index issu de la classe CouleurVetements
        for (CouleurVetements compteur : CouleurVetements.values()) {
            if (compteur.getIndex() == choixUtilisateur) {
                dernierChoixCouleur = compteur;
                break;
            }
        }
    }
}