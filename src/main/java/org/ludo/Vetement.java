package org.ludo;
import java.util.ArrayList;
import java.util.Scanner;

import static org.ludo.Chaussure.*;
import static org.ludo.Main.getJoueur;
public class Vetement {
    private static String typeDuVetementTop;
    private static String tailleDuVetementTop;
    private static String couleurDuVetementTop;
    private static String typeDuVetementBas;
    private static String tailleDuVetementBas;
    private static String couleurDuVetementBas;
    private static String choixDeLaTaille;

    /**cette méthode est une simple méthode pour l'affichage d'un texte qui fait l'introduction du menu d'habillement du personnage
     *
     */
    public static void menuIntroVetements(){
        System.out.println(" ");
        System.out.println("#############################################################################");
        System.out.println("Bienvenue dans la personnalisation de l'apparence vestimentaire du personnage");
        System.out.println("#############################################################################");
        System.out.println(" ");
        System.out.println("Vous allez devoir choisir le vetement du torse de votre personnage, son pantalon ainsi que ses Chaussures.");
        System.out.println(" ");
        System.out.println("Pour ce faire, vous choisirez le type, la taille et la couleur du vêtement, pour les chaussures le type de chaussures et leur couleurs.");
        System.out.println(" ");
    }
    /**Cette méthode sert à définir les caractéristiques du vetement Torse du personnage,
     * en incluant le type(typeDuVetementTop), la taille (tailleDuVetementTop) et la couleur (couleurDuVetementTop)
     */
    public static void menuVetementTop () {
        System.out.println("Veuillez choisir le haut de votre personnage ");
        //je créé une boucle afin d'énumérer les vetements qui sont dans la classe d'énumeration TypeVetement
        // qui soit uniquement porté sur le torse
        for (TypeVetement vetement : TypeVetement.values()) {
            if (vetement.getCategorie().equals("top")) {
                System.out.print(vetement.getNumero()+"-"+vetement.name() + " | ");
            }
        }
        System.out.println(" ");
        Scanner scanner = new Scanner(System.in);
        int numeroVetement = scanner.nextInt(); // recup numero
        //je créé une boucle for afin de sélectionner le type de vêtement sélectionné par l'utilisateur
        //et le sauvegarder dans la variable typeduvetementTop
        for (TypeVetement vetement : TypeVetement.values()) {
            if (vetement.getNumero() == numeroVetement) {
                String typeDuVetement = vetement.name();
                typeDuVetementTop=(typeDuVetement.toLowerCase());
                break;
            }
        }
        System.out.println( );
        System.out.println("Quelle taille de vêtement voulez vous utiliser ?");
        choixTaille();//j'appelle la fonction taille pour choisir la taille du vetêment et j'obtiens tailleDuVetementTop
        tailleDuVetementTop = choixDeLaTaille;
        System.out.println("De quelle couleur est le vêtement ?");
        Chaussure.nouveauChoixCouleur();//j'appelle la méthode nouveauChoixCouleur qui se trouve dans la classe chaussures pour obtenir couleurDuVetementTop
        couleurDuVetementTop = String.valueOf(Chaussure.getDernierChoixCouleur());
    }
    /**Cette méthode sert à définir les caractéristiques du vetement Pantalon du personnage,
     * en incluant le type(typeDuVetementBas), la taille (tailleDuVetementBop) et la couleur (couleurDuVetementBop)
     */
    public static void menuVetementBas() {

        System.out.println("Veuillez choisir le vêtement du bas de votre personnage ");
        //je créé une boucle afin d'énumérer les vetements qui sont dans la classe d'énumeration TypeVetement
        // qui soit uniquement porté sur le bas du personnage
        for (TypeVetement vetement : TypeVetement.values()) {
            if (vetement.getCategorie().equals("bas")) {
                System.out.print(vetement.getNumero()+"-"+vetement.name() + " | ");
            }
        }
        System.out.println(" ");
        Scanner scanner = new Scanner(System.in);
        int numeroVetement = scanner.nextInt(); // recup numero
        //Cette boucle for afin de sélectionner le type de vêtement sélectionné par l'utilisateur
        //et le sauvegarder dans la variable typeduvetementBas
        for (TypeVetement vetement : TypeVetement.values()) {
            if (vetement.getNumero() == numeroVetement) {
                String typeDuVetement = vetement.name();
                typeDuVetementBas=(typeDuVetement.toLowerCase());
                break;
            }
        }
        System.out.println("Quelle taille de vêtement voulez vous utiliser ?");
        choixTaille();//j'appelle la fonction taille pour choisir la taille du vetêment et j'obtiens tailleDuVetementBas
        tailleDuVetementBas = choixDeLaTaille;
        System.out.println("De quelle couleur est le vêtement ?");
        Chaussure.nouveauChoixCouleur();//appel de la méthode nouveauChoixCouleur qui se trouve dans la classe chaussures pour obtenir couleurDuVetementBas
        couleurDuVetementBas = String.valueOf(Chaussure.getDernierChoixCouleur());
    }
    /**Cette méthode utilise la classe d'énumération TailleVetement et en extraie les données pour définir la taille
     * du vetement choisi par l'utilisateur, quelque soit son emplacement (torse ou pantalon)
     */
    public static void choixTaille(){

        for (TailleVetement i : TailleVetement.values()) {
            System.out.print(i.getTailleIndex() + ". " + i.name() + "   |   ");
        }
        Scanner scanner = new Scanner(System.in);
        int choixUtilisateur = scanner.nextInt();
        //Je créé un for afin de faire un compteur, et comme cela, grâce à la condition, quand le compteur arrive au même chiffre
        // que celui entré par l'utilisateur, cela sélectionne la valeur de l'index issu de la classe CouleurVetements
        for (TailleVetement compteur : TailleVetement.values()) {
            if (compteur.getTailleIndex() == choixUtilisateur) {
                choixDeLaTaille = String.valueOf(compteur);
                break;
            }
        }
    }
    /**Cette méthode réunit les différentes informations récoltées sur les vêtements du Personnage
     */
    public static void panoplieComplete (){

        Chaussure.talonChaussures();
        System.out.println(" ");
        System.out.println("###   EN RESUME    ###");
        System.out.println("Le personnage porte en torse " + typeDuVetementTop + " de taille " + tailleDuVetementTop + " de couleur " + couleurDuVetementTop + ".");
        System.out.println("Le personnage porte en bas " + typeDuVetementBas + " de taille " + tailleDuVetementBas + " de couleur " + couleurDuVetementBas + ".");
        System.out.println("Le personnage porte des "+ getNomChaussure()+" de couleur " + getCouleurDesChaussures() + " avec un talon de " + getTalonChaussure() + " cm.");

    }
    /**Cette méthode sert à présenter un menu de fin de la section habillement du personnage.
     * On demande à l'utilisateur de valider son choix, qui sera alors sauvegardé, ou de recommencer s'il n'est pas satisfait
     */
    public static void menuOutro() {
        System.out.println("Etes-vous satisfait de votre sélection ? (o/n)");
        Scanner scanner = new Scanner(System.in);
        String choixUtilisateur = scanner.nextLine();
        if (choixUtilisateur.equalsIgnoreCase("o")) {
            System.out.println("Parfait, nous sauvegardons vos choix");
            sauvegarderChoix(); // j'appelle de la méthode pour sauvegarder les choix dans une liste
        } else {
            menuVetementTop();
            menuVetementBas();
            panoplieComplete();
            menuOutro();
        }
    }
    /**
     * Cette méthode sert à créer une liste des choix validés par l'utilisateur quant à tous ses choix vestimentaires.
     */
    public static void sauvegarderChoix() {
        getJoueur().chaussureList = new ArrayList<>();
        getJoueur().vetementList = new ArrayList<>();
        getJoueur().vetementList.add(typeDuVetementTop);
        getJoueur().vetementList.add(tailleDuVetementTop);
        getJoueur().vetementList.add(couleurDuVetementTop);
        getJoueur().vetementList.add(typeDuVetementBas);
        getJoueur().vetementList.add(tailleDuVetementBas);
        getJoueur().vetementList.add(couleurDuVetementBas);
        getJoueur().chaussureList.add(getNomChaussure());
        getJoueur().chaussureList.add(getCouleurDesChaussures());
        getJoueur().chaussureList.add(String.valueOf(getTalonChaussure()));
    }
}
